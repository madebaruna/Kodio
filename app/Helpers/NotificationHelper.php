<?php

namespace App\Helpers;

use App\Notification;
use App\User;

class NotificationHelper
{
    public static function kirim($type, User $tujuan, $isi)
    {
        $msg = [
            'user_id' => $tujuan->id,
            'read'    => 0
        ];

        if ($type == 'pesan') {
            $msg['pesan_id'] = $isi->id;
        } else if ($type == 'komentar') {
            $msg['comment_id'] = $isi->id;
        }

        Notification::create($msg);
    }

    public static function read(Notification $notification)
    {
        $notification->read = 1;
        $notification->save();
    }
}