<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Helpers\NotificationHelper;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VideoController extends Controller
{
    public function kategori($kategori)
    {
        $kategori = Category::where('slug', $kategori)->firstOrFail();
        $video    = $kategori->videos()->firstOrFail();

        return redirect('/' . $kategori->slug . '/' . $video->id);
    }

    public function play($kategori, $id)
    {
        $kategori = Category::where('slug', $kategori)->firstOrFail();
        $video    = $kategori->videos()->findOrFail($id);
        $videos   = $kategori->videos;
        $comments = $video->comments()->where('reply_id', null)
            ->with('replies')
            ->get();

        return view('video', [
            'videos'   => $videos,
            'video'    => $video,
            'kategori' => $kategori,
            'comments' => $comments
        ]);
    }

    public function comment($id, Request $request)
    {
        Video::findOrFail($id);

        $this->validate($request, [
            'komentar' => 'required|string'
        ]);

        Comment::create([
            'video_id' => $id,
            'user_id'  => Auth::user()->id,
            'reply_id' => null,
            'isi'      => $request->komentar,
            'like'     => 0,
            'dislike'  => 0
        ]);

        return redirect()->back();
    }

    public function reply($id, Request $request)
    {
        $comment = Comment::findOrFail($id);

        $this->validate($request, [
            'komentar' => 'required|string'
        ]);

        $reply = $comment->id;
        if ($comment->reply_id != null)
            $reply = $comment->reply_id;

        $komentar = Comment::create([
            'video_id' => $comment->video_id,
            'user_id'  => Auth::user()->id,
            'reply_id' => $reply,
            'isi'      => $request->komentar,
            'like'     => 0,
            'dislike'  => 0
        ]);

        NotificationHelper::kirim('komentar', $comment->user, $komentar);

        return redirect()->back();
    }

    public function like($comment)
    {
        $comment       = Comment::findOrFail($comment);
        $comment->like += 1;
        $comment->save();

        return redirect()->back();
    }

    public function dislike($comment)
    {
        $comment          = Comment::findOrFail($comment);
        $comment->dislike += 1;
        $comment->save();

        return redirect()->back();
    }
}
