<?php

namespace App\Http\Controllers;

use App\Category;
use App\Video;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $videos = Video::orderBy('id', 'desc')->paginate(20);

        return view('index', [
            'videos' => $videos
        ]);
    }

    public function kategori()
    {
        $kategori = Category::all();

        return view('kategori', [
            'kategori' => $kategori
        ]);
    }
}
