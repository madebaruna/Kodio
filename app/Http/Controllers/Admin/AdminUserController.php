<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Iklan;
use App\User;

class AdminUserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function index()
    {
        $users = User::orderBy('id', 'desc')->get();

        return view('admin.user', [
            'users' => $users,
        ]);
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->back();
    }
}
