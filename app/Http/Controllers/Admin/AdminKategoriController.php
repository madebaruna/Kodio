<?php

namespace App\Http\Controllers\admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminKategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    //tampilan list kategori
    public function index()
    {
        $categories = Category::orderBy('nama_kategori')->get();
        return view('admin.kategori.kategori', [
            'categories' => $categories
        ]);
    }

    //tambah kategori baru
    public function add(Request $request)
    {
        $this->validate($request, [
            'nama_kategori' => 'required|max:191|unique:categories,nama_kategori',
            'gambar_kategori' => 'required|max:191'
        ]);

        $path = $request->gambar_kategori->store('kategori', 'upload');

        Category::create([
            'nama_kategori' => $request->nama_kategori,
            'slug'          => str_slug($request->nama_kategori),
            'gambar'        => $path
        ]);

        return redirect()->back();
    }

    //hapus kategori
    public function delete($id)
    {
        $category = Category::findOrFail($id);

        $category->delete();

        return redirect()->back();
    }

    //halaman edit
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.kategori.kategori_edit', [
            'category' => $category
        ]);
    }

    //post edit
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_kategori' => 'required|max:191|unique:categories,nama_kategori'
        ]);

        $category = Category::findOrFail($id);

        $category->nama_kategori = $request->nama_kategori;
        $category->save();

        return redirect('/admin/kategori');
    }
}
