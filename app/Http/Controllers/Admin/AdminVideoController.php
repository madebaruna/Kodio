<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class AdminVideoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    //tampilkan halaman daftar video
    public function index()
    {
        $videos = Video::orderBy('id', 'desc')->paginate(10);
        return view('admin.video.video', [
            'videos' => $videos
        ]);
    }

    //tampilkan halaman daftar video menurut kategori
    public function showByCategory($kategori)
    {
        $category = Category::findOrFail($kategori);
        $videos = Video::where('category_id', $kategori)->orderBy('urutan')->orderBy('id', 'desc')->get();
        return view('admin.video.video_kategori', [
            'videos' => $videos,
            'category' => $category
        ]);
    }

    //tampilkan halaman tambah video
    public function add()
    {
        $c = Input::get('category_id');
        $categories = Category::orderBy('nama_kategori')->get();
        return view('admin.video.video_add', [
            'categories' => $categories,
            'currentCategory' => $c,
        ]);
    }

    //submit video
    public function post(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|string|max:191',
            'kode_video' => 'required|string',
            'kategori' => 'required|exists:categories,id'
        ]);

        Video::create([
            'judul' => $request->judul,
            'link' => $request->kode_video,
            'category_id' => $request->kategori,
            'urutan' => 0
        ]);

        return redirect('/admin/video');
    }

    //halaman edit
    public function edit($id)
    {
        $video = Video::findOrFail($id);
        $categories = Category::orderBy('nama_kategori')->get();
        return view('admin.video.video_edit', [
            'video' => $video,
            'categories' => $categories
        ]);
    }

    //edit video
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required|string|max:191',
            'kode_video' => 'required|string',
            'kategori' => 'required|exists:categories,id'
        ]);

        $video = Video::findOrFail($id);
        $video->judul = $request->judul;
        $video->link = $request->kode_video;
        $video->category_id = $request->kategori;
        $video->save();
        return redirect('/admin/video');
    }

    //hapus video
    public function delete($id)
    {
        $video = Video::findOrFail($id);

        $video->delete();

        return redirect()->back();
    }
}
