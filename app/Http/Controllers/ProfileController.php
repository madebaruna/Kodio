<?php

namespace App\Http\Controllers;

use App\User;
use App\Video;
use App\Comment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function profile()
    {
        return view('profile');
    }

    public function edit()
    {
        return view('edit');
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $validation = [
            'nama_depan' => 'required|string|max:255',
            'nama_belakang' => 'required|string|max:255',
            'username' => 'required|string|max:255',
            'tanggal_lahir' => 'string',
        ];

        $data = $request->all();

        if ($user->email != $data['email']) {
            $validation['email'] = 'required|string|email|max:191|unique:users';
        }
 
        if ($user->foto != null) {
            $validation['foto'] = 'image|max:2000';
        }

        $this->validate($request, $validation);
        $user->nama_depan    = $data['nama_depan'];
        $user->nama_belakang = $data['nama_belakang'];
        $user->username      = $data['username'];
        $user->email         = $data['email'];
        $user->tanggal_lahir = Carbon::createFromFormat("d/m/Y", $data['tanggal_lahir'])->format('Y-m-d');
        $user->biografi      = $data['biografi'];
        if ($request->hasFile('foto')) {
            $files      = $request->file('foto');
            $path       = $files->store('profile', 'uploads');
            $user->foto = $path;
        }
        $user->save();

        return redirect('/profile');
    }
}
