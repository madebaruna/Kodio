<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SearchController extends Controller
{
    public function search()
    {
        $query  = Input::get('q');
        $videos = Video::where('judul', "LIKE", "%$query%")->paginate(20);

        return view('search', [
            'videos' => $videos,
            'query'  => $query
        ]);
    }
}
