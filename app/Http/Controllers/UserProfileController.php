<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserProfileController extends Controller
{
    public function profile($id = null)
    {
        $edit = false;

        if ($id == null) {
            $user = Auth::user();
            $edit = true;
        } else
            $user = User::findOrFail($id);

        $comments = $user->comments()->orderBy('id', 'DESC')->get();

        return view('user.profile', [
            'comments' => $comments,
            'user'     => $user,
            'edit'     => $edit
        ]);

        return view('user.profile');
    }

    public function edit()
    {
        return view('user.edit');
    }

    public function update(Request $request)
    {
        $user       = Auth::user();
        $validation = [
            'nama_depan'    => 'required|string|max:191',
            'nama_belakang' => 'required|string|max:191',
            'biografi'      => 'string'
        ];

        $data = $request->all();

        if ($user->email != $data['email']) {
            $validation['email'] = 'required|string|email|max:191|unique:users';
        }

        if ($user->foto != null) {
            $validation['foto'] = 'image|max:2000';
        }

        $this->validate($request, $validation);
        $user->nama_depan    = $data['nama_depan'];
        $user->nama_belakang = $data['nama_belakang'];
        $user->email         = $data['email'];
        $user->biografi      = $data['biografi'];
        if ($request->hasFile('foto')) {
            $files      = $request->file('foto');
            $path       = $files->store('profile', 'upload');
            $user->foto = $path;
        }
        $user->save();

        return redirect('/profile');
    }
}
