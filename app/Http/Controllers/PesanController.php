<?php

namespace App\Http\Controllers;

use App\Foto;
use App\Helpers\NotificationHelper;
use App\Iklan;
use App\Kabupaten;
use App\Pesan;
use App\Provinsi;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PesanController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function get()
    {
        $user  = Auth::user();
        $pesan = DB::select("
            SELECT pengirim_id, penerima_id, MAX(created_at), MAX(isi) isi
            FROM pesan
            WHERE pengirim_id=$user->id OR penerima_id = $user->id
            GROUP BY (if(pengirim_id > penerima_id,  pengirim_id, penerima_id))
                    ,(if(pengirim_id > penerima_id,  penerima_id, pengirim_id))
            ORDER BY created_at DESC
        ");

        return view('pesan.daftar_pesan', [
            'messages' => $pesan,
        ]);
    }

    public function show($penerima)
    {
        $user  = Auth::user();
        $pesan = Pesan::where(function ($q) use (&$user, &$penerima) {
            return $q->where('pengirim_id', $user->id)->where('penerima_id', $penerima);
        })->orWhere(function ($q) use (&$user, &$penerima) {
            return $q->where('penerima_id', $user->id)->where('pengirim_id', $penerima);
        })->orderBy('created_at', 'DESC')->get();

        return view('pesan.pesan', [
            'pesan'  => $pesan,
            'target' => $penerima
        ]);
    }

    public function send(Request $request, $p)
    {
        $this->validate($request, [
            'pesan' => 'string'
        ]);

        $penerima = User::findOrFail($p);
        $user     = Auth::user();

        $pesan = Pesan::create([
            'pengirim_id' => $user->id,
            'penerima_id' => $penerima->id,
            'isi'         => $request->isi
        ]);

        NotificationHelper::kirim('pesan', $penerima, $pesan);

        $request->session()->flash('current', $penerima->id);

        return redirect()->back();
    }

    public function sendNew(Request $request, $p)
    {
        $penerima = User::findOrFail($p);

        $request->session()->flash('kirimbaru', $penerima->id);
        $request->session()->flash('current', $penerima->id);

        return redirect('/pesan/');
    }
}
