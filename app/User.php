<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'username', 'nama_depan', 'nama_belakang', 'email', 'password',
        'biografi', 'tanggal_lahir', 'foto', 'tipe'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function isAdmin()
    {
        return $this->tipe == 'admin';
    }

    public function getFotoAttribute($foto)
    {
        if ($foto == null) {
            return '/images/default-profile.png';
        } else {
            return '/images/' . $foto;
        }
    }
}
