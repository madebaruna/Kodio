<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'category_id',
        'link',
        'judul',
        'urutan'
    ];

    public function videos()
    {
        return $this->hasMany(Video::class)->orderBy('urutan');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
