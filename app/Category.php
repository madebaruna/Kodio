<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'nama_kategori',
        'slug',
        'gambar'
    ];

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function getGambarAttribute($gambar)
    {
        return '/images/' . $gambar;
    }
}
