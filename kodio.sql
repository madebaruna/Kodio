-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2017 at 07:51 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kodio`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_kategori` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `nama_kategori`, `slug`, `gambar`, `created_at`, `updated_at`) VALUES
(2, 'Java Dasar', 'java-dasar', '/kategori/java.jpg', '2017-05-16 09:47:26', '2017-05-16 10:00:32'),
(4, 'PHP', 'php', '/kategori/php.jpg', '2017-05-16 09:48:02', '2017-05-16 09:48:02'),
(5, 'Java OOP', 'java-oop', '/kategori/java.jpg', '2017-05-16 10:00:39', '2017-05-16 10:00:39'),
(6, 'Javascript', 'javascript', '/kategori/js.jpg', '2017-05-16 23:34:03', '2017-05-16 23:34:03');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `video_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `reply_id` int(10) UNSIGNED DEFAULT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `like` int(11) NOT NULL,
  `dislike` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `video_id`, `user_id`, `reply_id`, `isi`, `like`, `dislike`, `created_at`, `updated_at`) VALUES
(2, 2, 2, NULL, 'Download netbeans dimana?', 4, 0, '2017-05-25 12:15:28', '2017-05-25 13:52:27'),
(3, 2, 2, NULL, 'Apakah ngoding java harus pakai netbeans? soalnya semua di kampus pakai netbeans', 1, 1, '2017-05-25 12:40:57', '2017-05-25 13:53:42'),
(4, 2, 2, NULL, 'asdasdasdasd', 0, 3, '2017-05-25 13:43:23', '2017-05-25 13:53:45'),
(5, 2, 1, 2, 'https://netbeans.org/downloads/', 1, 0, '2017-05-25 14:01:48', '2017-05-25 14:30:53'),
(6, 2, 2, 2, 'Thanks admin', 0, 0, '2017-05-25 14:08:34', '2017-05-25 14:08:34'),
(7, 2, 1, 2, 'Sama sama', 0, 0, '2017-05-25 14:13:21', '2017-05-25 14:13:21'),
(8, 5, 2, NULL, 'Mantab!!!', 0, 0, '2017-05-25 14:14:04', '2017-05-25 14:14:04'),
(9, 8, 2, NULL, '???', 0, 1, '2017-05-25 14:16:28', '2017-05-25 14:16:32'),
(12, 2, 3, 3, 'bisa pakai notepad, intellij IDEA, eclipse', 1, 0, '2017-05-25 14:29:11', '2017-05-25 14:30:50'),
(13, 23, 3, NULL, 'halo halo', 0, 0, '2017-05-25 20:31:35', '2017-05-25 20:31:35'),
(14, 24, 1, NULL, 'hai', 0, 0, '2017-05-25 21:21:46', '2017-05-25 21:21:46'),
(15, 25, 1, NULL, 'hai', 2, 0, '2017-05-25 21:26:33', '2017-06-07 17:09:19'),
(17, 2, 3, 4, 'spam', 0, 0, '2017-06-07 09:24:09', '2017-06-07 09:24:09'),
(18, 19, 1, NULL, 'halo cinta', 2, 3, '2017-06-07 17:10:33', '2017-06-07 17:10:52'),
(19, 19, 1, 18, 'halo rangga', 0, 0, '2017-06-07 17:11:05', '2017-06-07 17:11:05'),
(20, 19, 1, 18, 'halo mawar', 0, 0, '2017-06-07 17:11:18', '2017-06-07 17:11:18'),
(21, 19, 1, NULL, 'halo cinta 2', 0, 0, '2017-06-07 17:11:43', '2017-06-07 17:11:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_05_09_190314_create_kategori_table', 1),
(4, '2017_05_09_190404_create_video_table', 1),
(5, '2017_05_09_190540_create_comment_table', 1),
(7, '2017_05_09_190617_create_report_table', 1),
(8, '2017_05_09_190592_create_pesan_table', 2),
(9, '2017_05_09_190602_create_notification_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `comment_id` int(10) UNSIGNED DEFAULT NULL,
  `pesan_id` int(10) UNSIGNED DEFAULT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `comment_id`, `pesan_id`, `read`, `created_at`, `updated_at`) VALUES
(1, 2, 17, NULL, 1, '2017-06-07 09:24:09', '2017-06-07 09:38:16'),
(2, 3, NULL, 2, 1, '2017-06-07 10:01:19', '2017-06-07 10:02:56'),
(4, 2, NULL, 4, 0, '2017-06-07 17:04:14', '2017-06-07 17:04:14'),
(5, 1, NULL, 5, 1, '2017-06-07 17:05:26', '2017-06-07 17:06:49'),
(6, 1, 19, NULL, 1, '2017-06-07 17:11:05', '2017-06-07 17:12:25'),
(7, 1, 20, NULL, 1, '2017-06-07 17:11:18', '2017-06-07 17:11:30'),
(8, 2, NULL, 6, 0, '2017-06-07 17:12:16', '2017-06-07 17:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('made.setia@gmail.com', '$2y$10$/tLtagqAi8Ch8oOVYoSbuOD.r54j2oXdLh42QiVAsVV2NS55r9DYi', '2017-05-16 23:55:29'),
('root@localhost.com', '$2y$10$.oMTjuutB4IyKBPPsrB3KOJVFfYJMnGlazYComguHaLcg4QepcmbK', '2017-05-17 00:21:39');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `pengirim_id` int(10) UNSIGNED NOT NULL,
  `penerima_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`pengirim_id`, `penerima_id`, `id`, `isi`, `created_at`, `updated_at`) VALUES
(3, 2, 1, 'Hello hello', '2017-06-07 16:51:56', '2017-06-07 16:51:56'),
(2, 3, 2, 'Halooo', '2017-06-07 10:01:19', '2017-06-07 10:01:19'),
(3, 2, 4, 'Jangan spam pls', '2017-06-07 17:04:14', '2017-06-07 17:04:14'),
(2, 1, 5, 'Halo pak admin', '2017-06-07 17:05:26', '2017-06-07 17:05:26'),
(1, 2, 6, 'halo cintra', '2017-06-07 17:12:16', '2017-06-07 17:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_komentar` int(10) UNSIGNED NOT NULL,
  `isi_laporan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_depan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_belakang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biografi` text COLLATE utf8mb4_unicode_ci,
  `tanggal_lahir` date DEFAULT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `nama_depan`, `nama_belakang`, `biografi`, `tanggal_lahir`, `foto`, `tipe`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@kodio.com', '$2y$10$DhMQcGJ1xP25aw2k/ZBayOontHjh4QOhqAfpaBTG3AsR5TdHrugVK', 'Admin', NULL, NULL, NULL, NULL, 'admin', 'tV6NUXoxVt2Ftwcn0tdHMz34cH1PRahSxYE18ieIht0iRH4MdUGTcsadqUOI', '2017-05-16 08:30:11', '2017-05-16 08:30:11'),
(2, 'madebaruna', 'root@localhost.com', '$2y$10$zHr5R/ezihrjUMG7AOy7nu1shBfLqkDWFvHyFrXsGtYxCCh08HBam', 'Made', 'Baruna', 'Halo, selamat malam semua. Semoga ilmu nya berguna bagi anda sekalian.!', NULL, 'profile/BbyBDgwencxpiQIF0lz1RLb8wPioVXk76mpsmZb8.jpeg', 'user', 'eHUoTwjZ3PIHlFAdhw3FOCZSytD7Egl7rKIqJsxYrGepnaemqh0leVe0Asfj', '2017-05-16 23:32:10', '2017-06-07 07:59:23'),
(3, 'halocinta', 'halocinta@gmail.com', '$2y$10$W5g5DwPqkNxadhjjC6zVj.ftiAvLFXUs9tZ/wOJ9jvQkHw7Eh8KnC', 'halo', 'cinta', NULL, NULL, '/profile/abcd123.png', 'user', '787ku7EYYUdSHqeVijGj9rAzxIWD0QmarmkOGrocCIbwXnthVkaU7p7vnHLt', '2017-05-17 23:41:35', '2017-05-17 23:41:35');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `category_id`, `link`, `judul`, `urutan`, `created_at`, `updated_at`) VALUES
(1, 2, 'qc3NAAeq6sc', 'Dasar Pemrograman - Variabel, Tipe Data, Standard I/O', NULL, '2017-05-17 05:47:42', '2017-05-17 05:47:42'),
(2, 2, 'O9ITpDu2Du0', 'Dasar Pemrograman Java - Seleksi Kondisi', NULL, '2017-05-17 06:09:27', '2017-05-17 06:09:27'),
(5, 2, 'FHGYFiwBhjI', 'Dasar Pemrograman Java - Looping', NULL, '2017-05-17 13:21:58', '2017-05-17 13:22:02'),
(6, 2, 'apSSYxCR2FY', 'Dasar Pemrograman Java - Method', NULL, '2017-05-17 13:22:00', '2017-05-17 12:08:06'),
(8, 2, 'lI-a9xmj6Wk', 'Dasar Pemrograman Java - Array', NULL, '2017-05-17 12:36:17', '2017-05-17 12:36:17'),
(9, 2, 'p-zl-vK8NXA', 'Dasar Pemrograman Java - File I/O Dasar', NULL, '2017-05-17 12:36:39', '2017-05-17 12:36:39'),
(10, 5, 'CT-TjS3M7qs', 'Java OOP Fundamental - Pengantar OOP, Class dan Object', NULL, '2017-05-17 12:38:06', '2017-05-17 12:44:26'),
(11, 5, 'VXZCTjSBhsY', 'Java OOP Fundamental - Access Modifier', NULL, '2017-05-17 12:44:51', '2017-05-17 12:44:51'),
(12, 5, '5uhR5CJntzg', 'Java OOP Fundamental - Class & Instance Member', NULL, '2017-05-17 12:45:16', '2017-05-17 12:45:16'),
(13, 5, 'vuhu6XLLnFI', 'Java OOP Fundamental - Overloading Method & Object Constructor', NULL, '2017-05-17 12:47:05', '2017-05-17 12:47:05'),
(14, 5, 'OdznXusWd24', 'Java OOP Fundamental - Encapsulation', NULL, '2017-05-17 12:47:22', '2017-05-17 12:47:22'),
(15, 5, '78ZIyuvBO4A', 'Java OOP Fundamental - Inheritance', NULL, '2017-05-17 12:47:38', '2017-05-17 12:47:38'),
(16, 5, 'l1lKiUsuMO4', 'Java OOP Fundamental - Interface', NULL, '2017-05-17 12:47:55', '2017-05-17 12:47:55'),
(17, 5, 'u_mqz3euXPc', 'Java OOP Fundamental - Polymorphism', NULL, '2017-05-17 12:48:10', '2017-05-17 12:48:10'),
(18, 5, 'JjUfVDXq588', 'Java OOP Fundamental - [Swing] Flow Layout', NULL, '2017-05-17 12:48:26', '2017-05-17 12:48:26'),
(19, 5, '2ciT4WTF6bo', 'Java OOP Fundamental - [Swing] Border Layout', NULL, '2017-05-17 12:48:45', '2017-05-17 12:48:45'),
(20, 5, 'TIO-UKEcjB8', 'Java OOP Fundamental - [Swing] Text Editor Sederhana', NULL, '2017-05-17 12:49:05', '2017-05-17 12:49:05'),
(21, 5, '3AOCcgKyIZg', 'Java OOP Fundamental - [Swing] GUI Layouting Menggunakan Netbeans', NULL, '2017-05-17 12:49:24', '2017-05-17 12:49:24'),
(22, 4, 'iCUV3iv9xOs', 'PHP Tutorial - Introduction to PHP', 0, '2017-05-25 20:28:19', '2017-05-25 20:28:19'),
(23, 4, 'k6ZiPqsBvEQ', 'PHP Tutorial - Installing XAMPP Part 1', 0, '2017-05-25 20:30:52', '2017-05-25 20:30:52'),
(24, 6, 'yQaAGmHNn9s', 'Introduction to JavaScript', 0, '2017-05-25 20:33:22', '2017-05-25 20:33:22'),
(25, 6, 'yUyJ1gcaraM', 'Comments and Statements', 0, '2017-05-25 20:33:39', '2017-05-25 20:33:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_video_id_foreign` (`video_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_reply_id_foreign` (`reply_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id_foreign` (`user_id`),
  ADD KEY `notifications_comment_id_foreign` (`comment_id`),
  ADD KEY `notifications_pesan_id_foreign` (`pesan_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pesan_pengirim_id_foreign` (`pengirim_id`),
  ADD KEY `pesan_penerima_id_foreign` (`penerima_id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_id_komentar_foreign` (`id_komentar`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `videos_id_kategori_foreign` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_reply_id_foreign` FOREIGN KEY (`reply_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_comment_id_foreign` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notifications_pesan_id_foreign` FOREIGN KEY (`pesan_id`) REFERENCES `pesan` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pesan`
--
ALTER TABLE `pesan`
  ADD CONSTRAINT `pesan_penerima_id_foreign` FOREIGN KEY (`penerima_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pesan_pengirim_id_foreign` FOREIGN KEY (`pengirim_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_id_komentar_foreign` FOREIGN KEY (`id_komentar`) REFERENCES `comments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `videos_id_kategori_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
