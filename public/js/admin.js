$(function () {
    /*
     Bagian Kategori
     */
    $('.hapus-kategori-btn').click(function () {
        var form = $('.hapus-kategori form');
        form.attr('action', form.data('url') + '/' + $(this).data('id'));
    });

    $('.hapus-video-btn').click(function () {
        var form = $('.hapus-video form');
        form.attr('action', form.data('url') + '/' + $(this).data('id'));
    });

    $('.hapus-user-btn').click(function () {
        var form = $('.hapus-user form');
        form.attr('action', form.data('url') + '/' + $(this).data('id'));
    });

    $(".kategori-select").select2({
        placeholder: "Kategori Video"
    });
});