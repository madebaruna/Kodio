$(function () {
    var scroll_start = 0;
    var logoBtn = $('.logo-btn');
    var logoSmall = $('.logo-small');
    var navbar = $('.navbar');

    var offset = 200;
    if (document.location.pathname !== '/')
        offset = -1;

    $(document).scroll(function () {
        scroll_start = $(this).scrollTop();
        if (scroll_start > offset) {
            logoBtn.css('width', '0');
            logoBtn.css('opacity', '0')
            logoSmall.css('width', '184px');
            logoSmall.css('opacity', '1')
            navbar.css('box-shadow', '0px 1px 6px 0px rgba(34, 34, 34, 0.56)');
        } else {
            logoBtn.css('width', '48px');
            logoBtn.css('opacity', '1')
            logoSmall.css('width', '0');
            logoSmall.css('opacity', '0')
            navbar.css('box-shadow', 'none');
        }
    });

    $('a.reply-comment').click(function (e) {
        $('.reply .comment-reply').remove();
        var commentBox = $('.comment-box').children();
        var linkReply = $(this).attr('href');
        $(this).parent().append(commentBox.clone()).find('form').attr('action', linkReply);

        e.preventDefault();
    })

    $('.reply').on('click', 'button.reply-cancel', function (e) {
        $('.reply .comment-reply').remove();
        e.preventDefault();
    })

    $('.reply-clear').click(function () {
        $('.comment-textarea').val('');
    });

    if ($('#pesan-identifier').length) {
        console.log('/pesan/' + $('#pesan-identifier').val());
        $.get('/pesan/' + $('#pesan-identifier').val(), function (res) {
            $('#isiPesan').html(res);
        });
    }

    $('.message-list').click(function (e) {
        $.get('/pesan/' + $(this).data('id'), function (res) {
            $('#isiPesan').html(res);
        });
        e.preventDefault();
    });
});