@extends('layout')

@section('title', 'Profile')

@section('content')
    <div class="container">
        <div class="row">
        <h1 style="text-align: center">Profile</h1>
        <hr>
            <div class="col-md-4 profile-container">
                 <div class="profile-userpic"> 
                    <img src="https://yt3.ggpht.com/-NgZ6iNsnFrg/AAAAAAAAAAI/AAAAAAAAAAA/rlX3rNt_q7U/s900-c-k-no-mo-rj-c0xffffff/photo.jpg">
                </div>
                <h3><i class="fa fa-user" aria-hidden="true"></i> {{ Auth::user()->username }}</h3>
                <p class="text-justify">{{ Auth::user()->biografi }}</p> 
                <hr>
                <h4><i class="fa fa-calendar"></i> {{ Auth::user()->tanggal_lahir }}</h4>
                <h4><i class="fa fa-envelope"></i> {{ Auth::user()->email }}</h4>          
                <a href="/profile/edit" class="btn btn-primary btn-lg edit-profile"><i class="glyphicon glyphicon-edit"></i> Edit Profile</a>
            </div>
            <div class="col-md-8 video-profile-container">
                <div class="video-play-profile">
                    <iframe src="https://www.youtube.com/embed/EaytPomTDWE" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection