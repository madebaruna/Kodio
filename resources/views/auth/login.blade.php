@extends('auth.layout')

@section('title', 'Login')

@section('content')
    <a href="/"><img class="logo" src="{{ asset('/img/Logo-small.png') }}" alt="Logo"></a>
    <div class="box">
        <h1>Login</h1>
        <form role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="text" class="form-control input-lg" name="email" value="{{ old('email') }}"
                       required autofocus placeholder="E-mail / username">

                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control input-lg" name="password" required
                    placeholder="Password">

                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg">
                    Login
                </button>
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember
                        Me
                    </label>
                </div>
            </div>

            <a class="btn btn-link forgot-password" href="{{ route('password.request') }}">
                Forgot Your Password?
            </a>
        </form>
    </div>

    <h4>Belum punya akun? <a class="register" href="{{ route('register') }}">Register</a></h4>
@endsection
