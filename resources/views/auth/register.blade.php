@extends('auth.layout')

@section('title', 'Register')

@section('content')
    <a href="/"><img class="logo" src="{{ asset('/img/Logo-small.png') }}" alt="Logo"></a>
    <div class="box">
        <h1>Register</h1>
        <form role="form" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form{{ $errors->has('nama_depan') ? ' has-error' : '' }}">
                        <input id="nama_depan" type="text" class="form-control input-lg" name="nama_depan"
                               value="{{ old('nama_depan') }}"
                               required autofocus placeholder="Nama depan">
                    </div>
                    @if ($errors->has('nama_depan'))
                        <span class="help-block">
                        <strong>{{ $errors->first('nama_belakang') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <div class="form-group form{{ $errors->has('nama_belakang') ? ' has-error' : '' }}">

                        <input id="email" type="text" class="form-control input-lg" name="nama_belakang"
                               value="{{ old('nama_belakang') }}"
                               required autofocus placeholder="Nama belakang">
                    </div>
                    @if ($errors->has('nama_belakang'))
                        <span class="help-block">
                        <strong>{{ $errors->first('nama_belakang') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <input id="email" type="text" class="form-control input-lg" name="username" value="{{ old('username') }}"
                       required autofocus placeholder="Username">

                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control input-lg" name="email" value="{{ old('email') }}"
                       required autofocus placeholder="E-email">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control input-lg" name="password" required
                       placeholder="Password">
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control input-lg" name="password_confirmation" required
                       placeholder="Konfirmasi password">

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }}">
                <input id="ttl" type="text" class="form-control input-lg" name="tanggal_lahir" required
                       placeholder="Tanggal Lahir">

                @if ($errors->has('tanggal_lahir'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tanggal_lahir') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg">
                    Register
                </button>
            </div>
        </form>
    </div>

    <h4>Sudah punya akun? <a class="register" href="{{ route('login') }}">Login</a></h4>
@endsection
