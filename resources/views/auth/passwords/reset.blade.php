@extends('auth.layout')

@section('title', 'Reset Password')

@section('content')
    <a href="/"><img class="logo" src="{{ asset('/img/Logo-small.png') }}" alt="Logo"></a>
    <div class="box">
        <h1>Reset password</h1>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control input-lg" name="email"
                       value="{{ $email or old('email') }}"
                       required autofocus placeholder="Email Address">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control input-lg" name="password" required
                placeholder="Password baru">

                @if ($errors->has('password'))
                    <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input id="password-confirm" type="password" class="form-control input-lg input-lg" name="password_confirmation"
                       required placeholder="Ulangi password baru">

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg">
                    Reset Password
                </button>
            </div>
        </form>
    </div>
@endsection
