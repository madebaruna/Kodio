@extends('auth.layout')

@section('title', 'Reset Password')

@section('content')
    <a href="/"><img class="logo" src="{{ asset('/img/Logo-small.png') }}" alt="Logo"></a>
    <div class="box">
        <h1>Reset password</h1>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control input-lg" name="email"
                       value="{{ old('email') }}" required placeholder="Email Address">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg">
                    Kirim Password Reset
                </button>
            </div>
        </form>
    </div>

    <h4>Belum punya akun? <a class="register" href="{{ route('register') }}">Register</a></h4>
@endsection
