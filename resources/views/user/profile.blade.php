@extends('layout')

@section('title', 'Profile User')

@section('content')
    <div class="container">
        <h1 style="text-align: center;">Profile</h1>
        <hr>
        <div class="row">
            <div class="profile-container col-md-4">
                <div class="profile-userpic">
                    <div class="picture"
                         style="background-image:url({{ $user->foto }});">
                    </div>
                </div>
                <p>
                <h4 style="text-align: center;">{{ "$user->nama_depan $user->nama_belakang" }}</h4>
                <p class="text-justify">{!! nl2br(e($user->biografi)) !!}</p>
                <hr>
                <p><i class="fa fa-envelope"></i> {{ $user->email }}</p>
                <p><i class="fa fa-calendar"></i> {{ (new \Carbon\Carbon($user->ttl))->format('d F Y') }}</p>
                @if($edit)
                    <a href="/profile/edit" class="btn btn-primary btn-lg edit-profile"
                       style="margin-top: 15px;"><i class="glyphicon glyphicon-edit"></i> Edit Profile</a>
                @endif
                @if(Auth::user() != null && Auth::user()->id != $user->id)
                    <a href="/pesan/kirim/{{ $user->id }}" class="btn btn-primary" style="display: block;"><i
                            class="fa fa-envelope"></i> Kirim Pesan</a>
                @endif
            </div>

            <div class="profile-container-comments col-md-8">
                @foreach($comments as $i)
                    <div class="box">
                        <div class="gambar"
                             style="background-image:url(//img.youtube.com/vi/{{ $i->video->link }}/mqdefault.jpg)">
                        </div>
                        <div class="detail">
                            <a style="display: inline-block; margin: 5px 0;"
                               href="/{{ $i->video->category->slug }}/{{ $i->video->id }}">{{ $i->video->judul }}</a>
                            <span style="padding: 5px;" class="detail-minor pull-right">
                            <i class="fa fa-clock-o"></i>
                                {{ (new \Carbon\Carbon($i->created_at))->format('d M Y H:m') }}
                            </span>
                            <hr>
                            <p>{{ $i->isi }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection('content')