@extends('layout')

@section('title', 'Edit Profile')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">

                <h1 style="text-align: center;">Edit Profile</h1>
                <hr>
                <div class="box-edit">
                    <div class="edit-profile-container">
                        <div class="profile-userpic">
                            <div class="picture"
                                 style="background-image:url({{ Auth::user()->foto }});">
                            </div>
                        </div>

                        <form action="/profile/update" role="form" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <label class="upload-foto" for="uploadFotoProfil">
                                <i class="fa fa-camera" aria-hidden="true"></i>
                                <input type="file" id="uploadFotoProfil" name="foto">
                                <span class="hidden">imagename</span>
                            </label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form{{ $errors->has('nama_depan') ? ' has-error' : '' }}">
                                        <input id="nama" type="text" class="form-control input-lg" name="nama_depan"
                                               value="{{ Auth::user()->nama_depan }}" placeholder="Nama depan">

                                        @if ($errors->has('nama_depan'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('nama_depan') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form{{ $errors->has('nama_belakang') ? ' has-error' : '' }}">
                                        <input id="nama" type="text" class="form-control input-lg" name="nama_belakang"
                                               value="{{ Auth::user()->nama_belakang }}" placeholder="Nama belakang">

                                        @if ($errors->has('nama_belakang'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('nama_belakang') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="form-control input-lg" name="email"
                                       value="{{ Auth::user()->email }}" placeholder="E-mail">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('biografi') ? ' has-error' : '' }}">
                                <textarea class="form-control" id="deskripsi" name="biografi"
                                          placeholder="Deskripsikan tentang anda..."
                                          rows="5">{{ Auth::user()->biografi }}</textarea>
                                @if ($errors->has('biografi'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('biografi') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <button class="btn btn-primary btn-lg edit-profile"><i class="glyphicon glyphicon-save"></i>
                                Simpan
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
@endsection