@extends('layout')

@section('title', 'Kategori')

@section('content')
    <div class="container">
        <h2>Pilih Kategori</h2>
        @foreach($kategori as $count => $k)
            @if(($count+1)%4 == 0)
                <div class="row">
                    @endif
                    <div class="col-md-3 video">
                        <a href="/{{ $k->slug }}">
                            <div class="video-thumbnail">
                                <img src="{{ $k->gambar }}">
                                <div class="video-hover">
                                    <i class="fa fa-play fa-5x"></i>
                                </div>
                            </div>
                        </a>
                        <a class="video-title" href="/{{ $k->slug }}">{{ $k->nama_kategori }}</a>
                    </div>
                    @if(($count+1)%4==0)
                </div>
            @endif
        @endforeach
    </div>
@endsection