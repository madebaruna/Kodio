@extends('layout')

@section('title', 'Pencarian')

@section('content')
    <div class="container">
        <h2>Hasil Pencarian {{ $query }}</h2>
        @foreach($videos as $count => $v)
            @if(($count+1)%4 == 0)
                <div class="row">
                    @endif
                    <div class="col-md-3 video">
                        <a href="/{{ $v->category->slug }}/{{ $v->id }}">
                            <div class="video-thumbnail">
                                <img src="//img.youtube.com/vi/{{ $v->link }}/mqdefault.jpg">
                                <div class="video-hover">
                                    <i class="fa fa-play fa-5x"></i>
                                </div>
                            </div>
                        </a>
                        <a class="video-title" href="/{{ $v->category->slug }}/{{ $v->id }}">{{ $v->judul }}</a>
                        <a class="video-kategori" href="/{{ $v->category->slug }}">{{ $v->category->nama_kategori }}</a>
                    </div>
                    @if(($count+1)%4==0)
                </div>
            @endif
        @endforeach
        <div class="row">
            <div class="col-md-12" style="display:block; margin: 0 auto;">
                {!! $videos->links() !!}
            </div>
        </div>
    </div>
@endsection