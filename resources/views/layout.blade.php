<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') - {{ config('app.name') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,600|Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-datetimepicker.min.css') }}">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top"
     style="{{ Route::current()->getName() != 'index' ? 'box-shadow: 0px 1px 6px 0px rgba(34, 34, 34, 0.56);' : ''}}">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle Nav</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                @if(Route::current()->getName() == 'index')
                    <img class="logo logo-btn" src="{{ asset('/img/Logo-btn.png') }}" style="width:auto" alt="logo">
                    <img class="logo logo-small" src="{{ asset('/img/Logo-small.png') }}" style="width:0" alt="logo">
                @else
                    <img src="{{ asset('/img/Logo-small.png') }}" alt="logo">
                @endif
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/kategori" role="button"><i class="fa fa-th-list" aria-hidden="true"></i> Kategori</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><span class="nav-spacer"></span></li>

                @if(Auth::user())
                    <li class="dropdown" style="display:inline-block;">
                        @php($notifications = \App\Notification::where('user_id', Auth::user()->id)->where('read', 0)->orderBy('id', 'desc')->get())
                        <a href="/login" class="dropdown-toggle" type="button"
                           id="headerNotification"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-bell-o"></i> {{ $notifications->count() != 0 ? $notifications->count() : ''}}
                        </a>
                        <ul class="dropdown-menu notif-box" aria-labelledby="headerNotification" style="width: 250px;">
                            <li class="dropdown-header"
                                style="text-align: center; font-size: 16px; color: #eee; padding-bottom: 0;">Notifikasi
                                Anda
                            </li>
                            <li role="separator" class="divider"></li>
                            @if($notifications->count() == 0)
                                <li><a href="#">Belum ada notifikasi</a></li>
                            @endif
                            @foreach($notifications as $notif)
                                <li>
                                    <a href="/readnotif/{{ $notif->id }}">
                                        @if($notif->comment_id != null)
                                            @php($comment = \App\Comment::find($notif->comment_id))
                                            <span>
                                        @if($notif->read == 0)
                                                    <span class="notif-new">baru</span>
                                                @endif
                                                Balasan di komentar anda</span><br/>
                                            <span class="notif-judul">{{ $comment->video->judul }}</span>
                                        @else
                                            @php($pesan = \App\Pesan::find($notif->pesan_id))
                                            <span>
                                        @if($notif->read == 0)
                                                    <span class="notif-new">baru</span>
                                                @endif
                                                Pesan baru dari</span><br/>
                                            {{ $pesan->user->nama_depan . ' ' . $pesan->user->nama_belakang }}
                                        @endif
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->nama_depan }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            @if(Auth::user()->isAdmin())
                                <li><a href="/admin"><i class="fa fa-dashboard"></i> Admin Dashboard</a></li>
                            @endif

                            <li><a href="/pesan"><i class="fa fa-envelope"></i> Pesan</a></li>
                            <li><a href="/profile"><i class="fa fa-user"></i> Profile</a></li>

                            <li role="separator" class="divider"></li>
                            <li><a href="/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
                        </ul>
                    </li>
                @else
                    <li><a href="/register">Daftar</a></li>
                    <li><a href="/login">Login</a></li>
                @endif
            </ul>
            <form class="navbar-form search-bar" method="GET" action="/search">
                <div class="form-group" style="display:inline;">
                    <div class="input-group" style="display:table;">
                        <input class="form-control search-input" name="q" placeholder="Search Here" autocomplete="off"
                               type="text">
                        <span class="input-group-btn" style="width:1%;">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</nav>
@yield('content')
<div class="footer">
    <div class="container">
        Copyright Kodio 2017
    </div>
</div>
<script src="{{ asset('/js/jquery.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/index.js') }}"></script>
<script src="{{ asset('/js/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('/js/auth.js') }}"></script>
</body>
</html>