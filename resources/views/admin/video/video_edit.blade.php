@extends('admin.layout')

@section('active-video', 'active')

@section('title', 'Tambah Video')

@section('content')
    <h2>Video</h2>
    <div class="box">
        <div class="box-header">
            <h3>Edit Video</h3>
        </div>
        <div class="box-content">
            <form method="POST" action="{{ url('/admin/video/edit/' . $video->id) }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
                    <label for="judul">Judul Video</label>
                    <input type="text" class="form-control" id="judul" name="judul"
                           placeholder="Judul Video"
                           value="{{ $video->judul }}" required>
                    @if ($errors->has('judul'))
                        <span class="help-block">
                            <strong>{{ $errors->first('judul') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('kode_video') ? ' has-error' : '' }}">
                    <label for="kode_video">Kode Video</label>
                    <input type="text" class="form-control" id="kode_video" name="kode_video"
                           placeholder="Kode Video"
                           value="{{ $video->link }}" required>
                    @if ($errors->has('kode_video'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kode_video') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('kategori') ? ' has-error' : '' }}">
                    <label for="kategori">Kategori</label>
                    <select class="form-control kategori-select" name="kategori">
                        <option value="" disabled selected>Kategori video</option>
                        @foreach($categories as $c)
                            <option {{ $video->category_id == $c->id ? 'selected' : '' }}
                                value="{{ $c->id }}">{{ $c->nama_kategori }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('kategori'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kategori') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</button>
                <a href="/admin/video" class="btn btn-default pull-right">Batal</a>
            </form>
        </div>
    </div>
@endsection
