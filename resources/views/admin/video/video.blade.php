@extends('admin.layout')

@section('active-video', 'active')

@section('title', 'Video')

@section('content')
    <h2 class="pull-left" style="display: inline-block">Daftar Video</h2>
    <a href="video/add" class="btn btn-primary tambah-video">Tambah Video</a>
    <div class="row">
        <table class="table table-hover">
            <thead>
            <tr>
                <th class="fit">ID</th>
                <th class="fit"></th>
                <th>Judul</th>
                <th>Kategori</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($videos) == 0)
                <tr>
                    <td class="fit" colspan="5">Belum ada video</td>
                </tr>
            @endif
            @foreach($videos as $no => $v)
                <tr>
                    <td>{{ $v->id }}</td>
                    <td><img src="//img.youtube.com/vi/{{ $v->link }}/default.jpg" alt=""></td>
                    <td>{{ $v->judul }}</td>
                    <td>{{ $v->category->nama_kategori }}</td>
                    <td class="fit">
                        <a href="video/edit/{{ $v->id }}" class="btn btn-xs btn-default">Edit</a>
                        <a data-id="{{ $v->id }}" href="#"
                           class="btn btn-xs btn-danger hapus-video-btn"
                           data-toggle="modal"
                           data-target=".hapus-video"
                        >
                            Hapus
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $videos->links() }}
    </div>

    <div class="modal fade hapus-video" tabindex="-1" role="dialog" aria-labelledby="hapusVideo">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus video ini?</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Apakah anda yakin ingin menghapus video ini?<br/>
                        Semua komentar dalam video ini juga akan terhapus!!
                    </p>
                </div>
                <div class="modal-footer">
                    <form data-url="{{ url('/admin/video/delete/') }}" method="POST" action="">
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>
                            Batal
                        </button>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
