@extends('admin.layout')

@section('title', 'Users')
@section('title-header', 'Daftar User')

@section('active-user', 'active')

@section('content')
    <h2>Daftar User</h2>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Email</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $u)
            <tr>
                <td class="fit">{{ $u->id }}</td>
                <td>{{ $u->name }}</td>
                <td>{{ $u->email }}</td>
                <td class="fit">
                    <button data-id="{{ $u->id }}" class="btn btn-xs btn-danger hapus-user-btn"
                            data-toggle="modal"
                            data-target=".hapus-user">
                        <i class="fa fa-trash"></i> Hapus
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="modal fade hapus-user" tabindex="-1" role="dialog" aria-labelledby="hapusUser">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus user ini?</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Apakah anda yakin ingin menghapus user ini?<br/>
                    </p>
                </div>
                <div class="modal-footer">
                    <form data-url="{{ url('/admin/user/delete/') }}" method="POST" action="">
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>
                            Batal
                        </button>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection