@extends('admin.layout')

@section('active-kategori', 'active')

@section('content')
    <h2>Edit Kategori</h2>
    <div class="box">
        <div class="box-header">
            <h3>Edit Kategori: {{ $category->nama_kategori }}</h3>
        </div>
        <div class="box-content">
            <form method="POST" action="{{ url('/admin/kategori/edit/' . $category->id) }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('nama_kategori') ? ' has-error' : '' }}">
                    <label for="nama_kategori">Nama Kategori</label>
                    <input type="text" class="form-control" id="nama_kategori" name="nama_kategori"
                           placeholder="Nama Kategori"
                           value="{{ $category->nama_kategori }}" required>
                    @if ($errors->has('nama_kategori'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama_kategori') }}</strong>
                        </span>
                    @endif
                </div>
                <a href="/admin/kategori" class="btn btn-default">Batal</a>
                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>
    </div>
@endsection
