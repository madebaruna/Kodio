@extends('admin.layout')

@section('active-kategori', 'active')

@section('content')
    <h2>Daftar Kategori</h2>
    <div class="row">
        <div class="col-md-7">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th class="fit">No</th>
                    <th>Nama</th>
                    <th>Jumlah Video</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if(count($categories) == 0)
                    <tr>
                        <td class="fit" colspan="3">Belum ada kategori</td>
                    </tr>
                @endif
                @foreach($categories as $no => $cat)
                    <tr>
                        <td>{{ $no+1 }}</td>
                        <td><a href="/admin/video/{{ $cat->id }}">{{ $cat->nama_kategori }}</a></td>
                        <td>{{ $cat->videos->count() }}</td>
                        <td class="fit">
                            <a href="kategori/edit/{{ $cat->id }}" class="btn btn-xs btn-default">Edit</a>
                            <a data-id="{{ $cat->id }}" href="#"
                               class="btn btn-xs btn-danger hapus-kategori-btn"
                               data-toggle="modal"
                               data-target=".hapus-kategori"
                            >
                                Hapus
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-5">
            <div class="box">
                <div class="box-header">
                    <h3>Tambah Kategori</h3>
                </div>
                <div class="box-content">
                    <form method="POST" action="{{ url('/admin/kategori/add') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('nama_kategori') ? ' has-error' : '' }}">
                            <label for="nama_kategori">Nama Kategori</label>
                            <input type="text" class="form-control" id="nama_kategori" name="nama_kategori"
                                   placeholder="Nama Kategori"
                                   value="{{ old('nama_kategori') }}" required>
                            @if ($errors->has('nama_kategori'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nama_kategori') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
                            <label for="gambar_kategori">Gambar Kategori</label>
                            <input type="file" class="form-control" id="gambar_kategori" name="gambar_kategori"
                                   placeholder="Gambar Kategori" required>
                            @if ($errors->has('gambar_kategori'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('gambar_kategori') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade hapus-kategori" tabindex="-1" role="dialog" aria-labelledby="hapusKategori">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Hapus kategori ini?</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Apakah anda yakin ingin menghapus kategori ini?<br/>
                        Semua video dalam kategori ini juga akan terhapus!!
                    </p>
                </div>
                <div class="modal-footer">
                    <form data-url="{{ url('/admin/kategori/delete/') }}" method="POST" action="">
                        {{ csrf_field() }}
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>
                            Batal
                        </button>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
