@extends('layout')

@section('title', 'Play Video')

@section('content')
    <div class="container video-main-play">
        <div class="row video-player">
            <div class="col-md-3">
                <div class="video-list-container">
                    <h4>{{ $kategori->nama_kategori }}</h4>
                    <hr>
                    <div class="video-list">
                        <ul>
                            @foreach($videos as $v)
                                <a href="/{{ $kategori->slug }}/{{ $v->id }}"
                                   class="video-item {{ $v->id == $video->id ? 'active' : ''}}">
                                    <li>
                                        {{ $v->judul }}
                                    </li>
                                </a>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="video-play">
                    <iframe src="https://www.youtube.com/embed/{{ $video->link }}" frameborder="0"
                            allowfullscreen></iframe>
                </div>
                <div class="komentar-video">
                    @if(Auth::user() != null)
                        <div class="row">
                            <div class="col-md-1">
                                <img src="{{ Auth::user()->foto }}">
                            </div>
                            <div class="col-md-11">
                                <form class="form" action="/comment/{{ $video->id }}" method="POST">
                                    {{ csrf_field() }}
                                    <textarea class="form-control comment-textarea" name="komentar"
                                              placeholder="Diskusi disini..."
                                              rows="3"></textarea>
                                    <button class="btn btn-primary pull-right">Kirim</button>
                                    <button type="button" class="btn btn-default pull-right reply-clear"
                                            style="margin-right: 10px;">Batal
                                    </button>
                                </form>
                            </div>
                        </div>
                    @endif
                    <hr>
                    @foreach($comments as $comment)
                        <ul class="comment">
                            <li>
                                <img src="{{ $comment->user->foto }}">
                                <span class="nama"><a
                                        href="/profile/{{ $comment->user->id }}">{{ $comment->user->username }}</a></span>
                                <span class="isi">
                                    {{ $comment->isi }}
                                    <span class="reply">
                                        @if(Auth::user() != null)
                                            <a class="reply-comment"
                                               href="/comment/{{ $comment->id }}/reply">balas</a> ▪
                                        @endif
                                        @php($likeCount = $comment->like - $comment->dislike)
                                        <span class="{{ $likeCount > 0 ? 'like-color' : '' }}">{{ $likeCount }}</span>
                                        <a href="/comment/{{ $comment->id }}/like"><i
                                                class="fa fa-thumbs-up"></i></a>
                                        <a href="/comment/{{ $comment->id }}/dislike"><i
                                                class="fa fa-thumbs-down"></i></a>
                                    </span>
                                    @foreach($comment->replies as $reply)
                                        <ul class="comment c-reply">
                                            <li>
                                                <img src="{{ $reply->user->foto }}">
                                                <span class="nama"><a
                                                        href="/profile/{{$reply->user->id}}">{{ $reply->user->username }}</a></span>
                                                <span class="isi">
                                                    {{ $reply->isi }}
                                                    <span class="reply">
                                                        @if(Auth::user() != null)
                                                            <a class="reply-comment"
                                                               href="/comment/{{ $reply->id }}/reply">balas</a> ▪
                                                        @endif
                                                        @php($likeCount = $reply->like - $reply->dislike)
                                                        <span
                                                            class="{{ $likeCount > 0 ? 'like-color' : '' }}">{{ $likeCount }}</span>
                                                        <a href="/comment/{{ $reply->id }}/like"><i
                                                                class="fa fa-thumbs-up"></i></a>
                                                        <a href="/comment/{{ $reply->id }}/dislike"><i
                                                                class="fa fa-thumbs-down"></i></a>
                                                    </span>
                                                </span>
                                            </li>
                                        </ul>
                                    @endforeach
                                </span>
                            </li>
                        </ul>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    @if(Auth::user() != null)
        <div class="comment-box hidden">
            <div class="row comment-reply">
                <div class="col-md-1" style="padding-right: 0px;">
                    <img src="{{ Auth::user()->foto }}">
                </div>
                <div class="col-md-11" style="padding-left: 0px;">
                    <form class="form" action="/comment/" method="POST">
                        {{ csrf_field() }}
                        <textarea class="form-control" name="komentar" placeholder="Diskusi disini..."
                                  rows="2"></textarea>
                        <button type="submit" class="btn btn-primary pull-right">Kirim</button>
                        <button type="button" class="btn btn-default pull-right reply-cancel"
                                style="margin-right: 10px;">Batal
                        </button>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection