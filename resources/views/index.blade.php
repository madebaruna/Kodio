@extends('layout')

@section('title', 'Home')

@section('content')
    <div class="jumbotron">
        <div class="container">
            <div class="description">
                <img class="big-picture" src="{{ asset('img/ORANG.png') }}">
                <img class="big-logo" src="{{ asset('/img/Logo.png') }}" alt="logo">
                <p>Belajar Ngoding & Diskusi Bareng GRATIS!</p>
                @if(Auth::user() == null)
                    <a class="btn btn-lg welcome-register" href="/register">Buat Akunmu Sekarang</a>
                    <a class="btn btn-lg welcome-register alternate" href="/kategori">Lihat Kategori Video</a>
                @endif
            </div>
        </div>
    </div>
    <div class="container">
        <h2>Video Terbaru</h2>
        @foreach($videos as $count => $v)
            @if(($count+1)%4 == 0)
                <div class="row">
                    @endif
                    <div class="col-md-3 video">
                        <a href="/{{ $v->category->slug }}/{{ $v->id }}">
                            <div class="video-thumbnail">
                                <img src="//img.youtube.com/vi/{{ $v->link }}/mqdefault.jpg">
                                <div class="video-hover">
                                    <i class="fa fa-play fa-5x"></i>
                                </div>
                            </div>
                        </a>
                        <a class="video-title" href="/{{ $v->category->slug }}/{{ $v->id }}">{{ $v->judul }}</a>
                        <a class="video-kategori" href="/{{ $v->category->slug }}">{{ $v->category->nama_kategori }}</a>
                    </div>
                    @if(($count+1)%4==0)
                </div>
            @endif
        @endforeach
        <div class="row">
            <div class="col-md-12" style="display:block; margin: 0 auto;">
                {!! $videos->links() !!}
            </div>
        </div>
    </div>
@endsection