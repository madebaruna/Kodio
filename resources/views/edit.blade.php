@extends('layout')

@section('title', 'Edit Profile')

@section('content')
<div class="container">
 <h1 style="text-align: center;">Edit Profile</h1>
    <hr>
    <div class="box-edit">
        <div class="edit-profile-container">
            <div class="profile-userpic"> 
                    <img src="https://yt3.ggpht.com/-NgZ6iNsnFrg/AAAAAAAAAAI/AAAAAAAAAAA/rlX3rNt_q7U/s900-c-k-no-mo-rj-c0xffffff/photo.jpg">
            </div>

            <form action="/profile/update" role="form" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <label class="upload-foto" for="uploadFotoProfil">
                    <i class="fa fa-camera" aria-hidden="true"></i>
                    <input type="file" id="uploadFotoProfil" name="foto">
                    <span class="hidden">imagename</span>
                </label>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form{{ $errors->has('nama_depan') ? ' has-error' : '' }}">
                        <input id="nama_depan" type="text" class="form-control input-lg" name="nama_depan"
                               value="{{ Auth::user()->nama_depan }}"
                               required autofocus placeholder="Nama depan">
                    </div>
                    @if ($errors->has('nama_depan'))
                        <span class="help-block">
                        <strong>{{ $errors->first('nama_belakang') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="col-md-6">
                    <div class="form-group form{{ $errors->has('nama_belakang') ? ' has-error' : '' }}">

                        <input id="email" type="text" class="form-control input-lg" name="nama_belakang"
                               value="{{ Auth::user()->nama_belakang }}"
                               required autofocus placeholder="Nama belakang">
                    </div>
                    @if ($errors->has('nama_belakang'))
                        <span class="help-block">
                        <strong>{{ $errors->first('nama_belakang') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

                <div class="form-group form{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="nama" type="text" class="form-control input-lg" name="username"
                           value="{{ Auth::user()->username }}" placeholder="Username">

                    @if ($errors->has('name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control input-lg" name="email"
                           value="{{ Auth::user()->email }}" placeholder="E-mail">

                    @if ($errors->has('email'))
                        <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }}">
                    <input id="ttl" type="text" class="form-control input-lg" name="tanggal_lahir"
                           value="{{ str_replace('-', '/', Auth::user()->tanggal_lahir) }}" placeholder="Tanggal Lahir">

                    @if ($errors->has('tanggal_lahir'))
                        <span class="help-block">
                        <strong>{{ $errors->first('tanggal_lahir') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
            <textarea class="form-control" id="deskripsi" name="biografi" placeholder="Tuliskan biografi anda..."
                      rows="5">{{ Auth::user()->biografi }}</textarea>
                    @if ($errors->has('deskripsi'))
                        <span class="help-block">
                        <strong>{{ $errors->first('deskripsi') }}</strong>
                </span>
                    @endif
                </div>
                <button class="btn btn-primary btn-lg edit-profile"><i class="glyphicon glyphicon-save"></i>
                    Simpan
                </button>
            </form>
        </div>
    </div>
</div>
@endsection