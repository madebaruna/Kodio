@extends('layout')

@section('title', 'Pesan')

@section('content')
    <div class="container">
        <h2 class="kategori-title">Daftar Pesan</h2>
        <hr>
        <div class="pesan-box">
            <div class="row">
                <div class="col-md-4">
                    <ul class="nav nav-pills nav-stacked">
                        @if(session('kirimbaru') != null)
                            <li role="presentation" style="border-bottom: 1px solid #eee;">
                                <a>
                                <span
                                    style="color: black !important;">Kirim pesan baru ke {{ \App\User::find(session('kirimbaru'))->nama_depan }}</span>
                                </a>
                            </li>
                        @endif

                        @foreach($messages as $m)
                            <li role="presentation" style="border-bottom: 1px solid #eee;">
                                @if(Auth::user()->id == $m->pengirim_id)
                                    <a href="#" data-id="{{ $m->penerima_id }}" class="message-list">
                                        @php($u = \App\User::find($m->penerima_id))
                                        {{ "$u->nama_depan $u->nama_belakang" }}
                                    </a>
                                @else
                                    <a href="#" data-id="{{ $m->pengirim_id }}" class="message-list">
                                        @php($u = \App\User::find($m->pengirim_id))
                                        {{ "$u->nama_depan $u->nama_belakang" }}
                                    </a>
                                @endif

                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-8" id="isiPesan">
                    @if(session('current') != null)
                        <input type="hidden" id="pesan-identifier" value="{{ session('current') }}">
                    @else
                        @if(count($messages) == 0)
                            <h4>Belum ada pesan</h4>
                        @else
                            <h4>Klik pesan disamping untuk melihat isinya.</h4>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection