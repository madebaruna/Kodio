<form action="/pesan/{{ $target }}" method="POST">
    {{ csrf_field() }}
    <div class="form-group pesan-form">
        <label for="isi-komentar">Kirim pesan baru</label>
        <textarea class="form-control" id="isi-komentar" name="isi" rows="3"></textarea>
        <button class="btn btn-primary kirim">Kirim</button>
    </div>
</form>
@foreach($pesan as $p)
    <div class="pesan">
        <a href="/profile/{{ $p->pengirim_id }}">{{ $p->user->nama_depan . " " . $p->user->nama_belakang }}</a>
        <span class="pull-right">{{ $p->created_at }}</span>
        <p style="margin: 5px 0 0 0;">{{ $p->isi }}</p>
    </div>
@endforeach