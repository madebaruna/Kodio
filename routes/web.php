<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/readnotif/{id}', 'NotificationController@read');
Route::get('/search', 'SearchController@search');

Route::get('/kategori', 'IndexController@kategori');
Route::post('/comment/{id}', 'VideoController@comment');
Route::get('/comment/{comment}/like', 'VideoController@like');
Route::get('/comment/{comment}/dislike', 'VideoController@dislike');
Route::post('/comment/{comment}/reply', 'VideoController@reply');

Route::group(['prefix' => 'pesan'], function () {
    Route::get('/', 'PesanController@get');
    Route::get('/kirim/{p}', 'PesanController@sendNew');
    Route::get('/{penerima}', 'PesanController@show');
    Route::post('/{p}', 'PesanController@send');
});

Route::group(['prefix' => 'profile'], function () {
    Route::get('/', 'UserProfileController@profile')->middleware('auth');
    Route::get('/edit', 'UserProfileController@edit');
    Route::post('/update', 'UserProfileController@update');
    Route::get('/{id}', 'UserProfileController@profile');
});
/*`
 * Route untuk bagian admin
 */
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'Admin\AdminController@index');

    Route::group(['prefix' => 'kategori'], function () {
        Route::get('/', 'Admin\AdminKategoriController@index');
        Route::post('/add', 'Admin\AdminKategoriController@add');
        Route::post('/delete/{id}', 'Admin\AdminKategoriController@delete');
        Route::get('/edit/{id}', 'Admin\AdminKategoriController@edit');
        Route::post('/edit/{id}', 'Admin\AdminKategoriController@update');
    });

    Route::group(['prefix' => 'video'], function () {
        Route::get('/', 'Admin\AdminVideoController@index');
        Route::get('/add', 'Admin\AdminVideoController@add');
        Route::post('/add', 'Admin\AdminVideoController@post');
        Route::post('/delete/{id}', 'Admin\AdminVideoController@delete');
        Route::get('/edit/{id}', 'Admin\AdminVideoController@edit');
        Route::post('/edit/{id}', 'Admin\AdminVideoController@update');
        Route::get('/{kategori}', 'Admin\AdminVideoController@showByCategory');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'Admin\AdminUserController@index');
        Route::post('/delete/{id}', 'Admin\AdminUserController@delete');
    });
});

Route::get('/{kategori}/{id}', 'VideoController@play');
Route::get('/{kategori}', 'VideoController@kategori');
